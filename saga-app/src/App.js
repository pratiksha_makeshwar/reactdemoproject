import "./App.css";
import { useDispatch, useSelector } from "react-redux";
import { getUsersFetch } from "./actions";

function App() {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.myfirstReducer.users);

  return (
    <div className="App">
      <button onClick={() => dispatch(getUsersFetch())}>User List</button>
      <div>
        Users:{" "}
        {users.map((user) => (
          <div>{user.name}</div>
        ))}
      </div>
    </div>
  );
}

export default App;
