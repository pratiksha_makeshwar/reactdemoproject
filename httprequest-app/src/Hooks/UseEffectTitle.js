import React, {useEffect, useState} from 'react'

function UseEffectTitle() {
    const [count, setCount] = useState(0);
    const [name, setName] = useState('')
 
    useEffect(() => {
        console.log('useEffect - updateing the documnet title')
       document.title = `You Clicked ${count} Times`
    }, [count])
    return (
        <div>
        <input type='text' value={name} onChange={e => setName(e.target.value)} />
           <button onClick={() => setCount(count + 1)}>Click {count} Times</button> 
        </div>
    )
}

export default UseEffectTitle
