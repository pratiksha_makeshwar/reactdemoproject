import React, { useState } from 'react'

 function UseStateCounter() {
   //  const [count, setCount] = useState(0);
   const [name, setName] = useState({firstName: '', lastName:''})
    return (
        <div>
            {/* <button onClick={() => setCount(count + 1)}>count {count}</button> */}
            <form>
                <div>
                    <input name='firstname' type='text' value={name.firstName} 
                    onChange={e => setName({...name, firstName : e.target.value})} />
                    <input name='lastname' type='text' value={name.lastName} 
                    onChange={e => setName({...name, lastName : e.target.value})} />
                </div>
                <h2>FirstName: {name.firstName}</h2>
                <h2>Your Last Name: {name.lastName}</h2>
            </form>
        </div>
    )
}

export default UseStateCounter;


