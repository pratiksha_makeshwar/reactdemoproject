import React, { Component } from "react";
import UpdatedComponent from "./HOC";

class HoverCounter extends Component {
  render() {
    const { count, clickcountHandler } = this.props;
    return (
      <div>
        <h2 onMouseOver={clickcountHandler}>Hover click {count} Times</h2>
      </div>
    );
  }
}

export default UpdatedComponent(HoverCounter, 2);
