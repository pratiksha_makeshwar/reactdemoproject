import React, { Component } from "react";

class UserGreeetings extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLooged: false,
    };
  }

  render() {
    return this.state.isLooged ? <div>Welcome Gaurav</div> : <div>Bye</div>;
  }
}

export default UserGreeetings;
