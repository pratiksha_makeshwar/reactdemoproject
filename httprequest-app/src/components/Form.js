import React, { Component } from "react";

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      comments: "",
      Topic: "",
    };
  }
  namechangeHandler = (event) => {
    this.setState({
      username: event.target.value,
    });
  };

  commentchangeHandler = (event) => {
    this.setState({
      comments: event.target.value,
    });
  };

  topicchangeHandler = (event) => {
    this.setState({
      Topic: event.target.value,
    });
  };
  handleSubmit = (event) => {
    alert(`${this.state.username} ${this.state.comments} ${this.state.Topic}`);
    event.preventDefault();
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <h2>Form Component</h2>
          <label>Username</label>
          <input
            type="text"
            value={this.state.username}
            onChange={this.namechangeHandler}
          />
        </div>
        <div>
          <label>Comments</label>
          <textarea
            type="text"
            value={this.state.comments}
            onChange={this.commentchangeHandler}
          ></textarea>
        </div>
        <div>
          <label>Topic</label>
          <select value={this.state.Topic} onChange={this.topicchangeHandler}>
            <option value="React">React</option>
            <option value="Angular">Angular</option>
            <option value="Js">Js</option>
          </select>
        </div>
        <button>Submit</button>
      </form>
    );
  }
}

export default Form;
