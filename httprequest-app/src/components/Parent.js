import React, { Component } from "react";
import RegComponent from "./RegComponent";
import Pure from "./Pure";

class Parent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "Raju",
    };
  }
  componentDidMount() {
    this.setState = {
      name: "Raju",
    };
  }

  render() {
    console.log("Parent Comp");
    return (
      <div>
        Parent Component
        <RegComponent name={this.state.name} />
        <Pure name={this.state.name} />
      </div>
    );
  }
}

export default Parent;
