import React from "react";
import "./StyleSheet.css";
function StyleSheet() {
  const header = {
    color: "pink",
  };
  return (
    <div>
      <h2 style={header}>StyleSheet</h2>
    </div>
  );
}

export default StyleSheet;
