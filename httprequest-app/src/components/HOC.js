import React, { Component } from "react";

const UpdatedComponent = (WrappedComponent, incrementNumber) => {
  class NewComponent extends Component {
    constructor(props) {
      super(props);

      this.state = {
        count: 0,
      };
    }
    clickcountHandler = () => {
      this.setState((prevcount) => {
        return {
          count: prevcount.count + incrementNumber,
        };
      });
    };
    render() {
      return (
        <WrappedComponent
          count={this.state.count}
          clickcountHandler={this.clickcountHandler}
          {...this.props}
        />
      );
    }
  }
  return NewComponent;
};
export default UpdatedComponent;
