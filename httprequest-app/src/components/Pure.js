import React, { PureComponent } from "react";
import RegComponent from "./RegComponent";
import Parent from "./Parent";

class Pure extends PureComponent {
  render() {
    console.log("Pure Component");
    return <div>Pure Component {this.props.name}</div>;
  }
}

export default Pure;
