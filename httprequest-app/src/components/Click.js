import React, { Component } from "react";
import UpdatedComponent from "./HOC";

class Click extends Component {
  render() {
    const { count, clickCountHandler } = this.props;
    return (
      <button onClick={clickCountHandler}>
        {this.props.name} click {count} times
      </button>
    );
  }
}

export default UpdatedComponent(Click, 5);
