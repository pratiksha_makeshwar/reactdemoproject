import React from "react";
import Person from "./Person";

function NameList() {
  const persons = [
    {
      id: 1,
      name: "Pratik",
      age: 23,
    },
    {
      id: 2,
      name: "Pratiksha",
      age: 22,
    },
  ];
  const PersonList = persons.map((person) => (
    <Person key={person.id} person={person}></Person>
  ));
  return <div>{PersonList}</div>;
}

export default NameList;
