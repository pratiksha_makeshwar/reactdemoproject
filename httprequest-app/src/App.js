import logo from './logo.svg';
import './App.css';
import React, { Component, PureComponent } from 'react';
import PostList from './PostList';
import Message from './components/Message';
import Counter from './components/Counter';
import ParentComponent from './components/ParentComponent';
import UserGreeetings from './components/UserGreeetings';
import NameList from './components/NameList';
import StyleSheet from './components/StyleSheet';
import Form from './components/Form'
import Pure from './components/Pure';
import Parent from './components/Parent';
import Hero from './components/Hero'
import ErrorBoundary from './components/ErrorBoundary';
import Click from './components/Click'
import HoverCounter from './components/HoverCounter';
import PostForm from './PostForm';
import UseStateCounter from './Hooks/UseStateCounter';
import UseEffectTitle from './Hooks/UseEffectTitle';
import UseReducerCounter from './Hooks/UseReducerCounter';


class App extends Component {
 

  render() {
  return (
    <div className="App">
    {/* <UseStateCounter /> */}
   {/* <UseEffectTitle /> */}
   <UseReducerCounter />
   <Form />
    </div>
  );
}
}

export default App;




//  <ErrorBoundary>
//     <Hero Heroname="Vikas" />
//     <Hero Heroname="Shila" />
//     <Hero Heroname="Joker" />
//     </ErrorBoundary>
//     <Parent /> 
//     <Form />
//     <StyleSheet />
//     <NameList />
//     <UserGreeetings />
//      <PostList />
//      <Message />
//      <Counter/>
//      <ParentComponent />
//       <PostList /> //      <Click  name='Pratik'/>
//       <HoverCounter /> 