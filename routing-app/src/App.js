import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import "./App.css";
import Home from "./Pages/Home";
import Navbar from "./components/Navbar";
import Department from "./Pages/Department";
import ProductWithid from "./Pages/ProductWithid";
import Profilepage from "./Pages/Profilepage";
import Notfound from "./Pages/Notfound";

function App() {
  const isAuthenticate = true;
  return (
    <div className="App">
      <h1>Hello</h1>

      <BrowserRouter>
        <Navbar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/department" component={Department} />
          <Route path="/department/:productid" component={ProductWithid} />
          {/* <Route path='/profile' component={Profilepage}/>*/}
          <Route
            path="/profile"
            render={() => {
              return isAuthenticate ? <Profilepage /> : <Redirect to="/" />;
            }}
          />
          <Route path="*" component={Notfound} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
