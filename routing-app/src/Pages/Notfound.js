import React from "react";

function Notfound() {
  return (
    <div>
      <h1>404 | Not Found Page</h1>
    </div>
  );
}

export default Notfound;
