import React from "react";
import { useParams } from "react-router";

function ProductWithid() {
  const { productid } = useParams();
  return <h3>Individual Product Page: {productid}</h3>;
}

export default ProductWithid;
