import React from "react";
import { Link, useHistory } from "react-router-dom";

function Department() {
  const history = useHistory();
  return (
    <div>
      <h1>Hello Department</h1>
      <Link to="/">Home</Link>

      <button onClick={() => history.push("/department/123")}>
        go to product with id: 123
      </button>
      <button onClick={() => history.replace("/department/456")}>
        go to product with id: 456
      </button>
    </div>
  );
}

export default Department;
