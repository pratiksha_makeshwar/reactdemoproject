import Classes from "./App.css";
import React, { PureComponent } from "react";
import Persons from "../Components/Persons/Persons";
import Cockpit from "../Components/Cockpit/Cockpit";

class App extends PureComponent {
  constructor(props) {
    super(props);
    console.log("[App.js] Inside Constructor", props);

    this.state = {
      persons: [
        { id: "abc1", name: "Max", age: 12 },
        { id: "def2", name: "Manu", age: 24 },
        { id: "ghi1", name: "Stepend", age: 25 },
      ],
      otherState: "other value",
      showPersons: false,
      toogleClicked: 0,
    };
  }

  componentDidMount() {
    console.log("[App.js] inside componentDidMount()");
  }

  componentWillUpdate(nextProps, nextState) {
    console.log(
      "[Update App.js] Inside ComponentWillUpdate()",
      nextProps,
      nextState
    );
  }
  componentDidUpdate() {
    console.log("[Update App.js] Inside ComponentDidUpdate()");
  }

  nameChangedHandler = (event, id) => {
    const list = this.state.persons;
    const personIndex = list.findIndex((p) => {
      return p.id === id;
      //to find the element at that index
    });
    //create object to store element at index and used spread operator to make of object
    const person = {
      ...list[personIndex],
    };
    //Assign person.name to changed name at that event
    person.name = event.target.value;

    //Now to have to update array
    const persons = [...list];

    //Now we have to update one position to updated person
    persons[personIndex] = person;

    //setState to updated persons
    this.setState({
      persons: persons,
    });
  };
  deletePersonHandler = (personIndex) => {
    const persons = this.state.persons.slice();
    persons.splice(personIndex, 1);
    this.setState({ persons: persons });
  };
  togglePersonsHandler = () => {
    const DoesShow = this.state.showPersons;
    this.setState((prevState, props) => {
      return {
        showPersons: !DoesShow,
        toogleClicked: prevState.toogleClicked + 1,
      };
    });
  };
  render() {
    console.log("[App.js] Inside Render");

    let persons = null;
    if (this.state.showPersons) {
      persons = (
        <Persons
          persons={this.state.persons}
          clicked={this.deletePersonHandler}
          changed={this.nameChangedHandler}
        />
      );
    }

    return (
      <div className="App">
        <button
          onClick={() => {
            this.setState({ showPersons: true });
          }}
        >
          Show Person
        </button>
        <Cockpit
          apptitle={this.props.title}
          persons={this.state.persons}
          clicked={this.togglePersonsHandler}
        />

        {persons}
      </div>
    );
  }
}

export default App;
