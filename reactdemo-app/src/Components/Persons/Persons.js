import React, { PureComponent } from "react";
import Person from "./Person/Person";

class Persons extends PureComponent {
  constructor(props) {
    super(props);
    console.log("[Persons.js] Inside Constructor", props);
  }

  componentDidMount() {
    console.log("[Persons.js] inside componentDidMount()");
  }
  componentWillReceiveProps(nextProps) {
    console.log(
      "[Update persons.js] Inside componentWillReceiveProps()",
      nextProps
    );
  }

  componentWillUpdate(nextProps, nextState) {
    console.log(
      "[Update persons.js] Inside ComponentWillUpdate()",
      nextProps,
      nextState
    );
  }
  componentDidUpdate() {
    console.log("[Update persons.js] Inside ComponentDidUpdate()");
  }

  render() {
    const PersonList = this.props.persons.map((person, index) => {
      return (
        <Person
          click={() => this.props.clicked(index)}
          name={person.name}
          age={person.age}
          key={person.id} //used to update list efficiently
          changed={(event) => this.props.changed(event, person.id)}
        />
      );
    });
    console.log("[Persons.js] inside Render()");
    return <React.Fragment>{PersonList}</React.Fragment>;
  }
}
export default Persons;
