import React, { Component } from "react";

import PropTypes from "prop-types";
import PersonC from "./Person.css";

class Person extends Component {
  constructor(props) {
    super(props);
    console.log("[Person.js] Inside Constructor", props);
  }

  componentDidMount() {
    console.log("[Person.js] inside componentDidMount()");
  }
  render() {
    console.log("[Person.js] Inside Render()");
    return (
      <div className="Person">
        <p onClick={this.props.click}>
          My name is {this.props.name} and I am {this.props.age} years old!{" "}
        </p>
        <p>{this.props.children}</p>
        <input
          type="text"
          onChange={this.props.changed}
          value={this.props.name}
        />
      </div>
    );
  }
}

Person.propTypes = {
  click: PropTypes.func,
  name: PropTypes.string,
  age: PropTypes.number,
  changed: PropTypes.func,
};

export default Person;
