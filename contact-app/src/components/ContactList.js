import React from "react";
import CardContact from "./CardContact";
import { Link } from "react-router-dom";

const ContactList = (props) => {
  console.log(props);

  const deletecontactHandler = (id) => {
    props.getContactId(id);
  };

  const renderContact = props.contacts.map((contact) => {
    return (
      <CardContact
        contact={contact}
        clickHandler={deletecontactHandler}
        key={contact.id}
      ></CardContact>
    );
  });

  return (
    <div className="main">
      <h1>List</h1>
      <Link to="/add">
        <button className="ui button blue right"> Add Contact</button>
      </Link>

      <div className="ui celles list">{renderContact}</div>
    </div>
  );
};

export default ContactList;
