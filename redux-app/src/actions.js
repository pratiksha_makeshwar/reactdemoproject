export const GET_USERS_SUCCESS = "GET_USERS_SUCCESS";
export const GET_USERS_FETCH = "GET_USERS_FETCH";

export const getUsersFetch = () => ({
  type: GET_USERS_FETCH,
});
