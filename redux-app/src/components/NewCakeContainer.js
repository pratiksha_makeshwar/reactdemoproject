import React, { useState } from "react";
import { buyCake } from "../redux/Actions/CakeAction";
import { connect } from "react-redux";

const NewCakeContainer = (props) => {
  const [number, SetNumber] = useState(1);
  return (
    <div>
      <h2> Number Of Cakes- {props.numberOfCakes}</h2>
      <input
        type="text"
        value={number}
        onChange={(e) => SetNumber(e.target.value)}
      />
      <button onClick={() => props.buy(number)}>Buy {number} Cake</button>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    numberOfCakes: state.cake.numberOfCakes,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    buy: (number) => dispatch(buyCake(number)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewCakeContainer);
