import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { buyCake } from "../redux/Actions/CakeAction";

const HooksCakeContainer = () => {
  const numberOfCakes = useSelector((state) => state.cake.numberOfCakes);
  const dispatch = useDispatch();
  return (
    <div>
      <h2>Num Of Cakes:{numberOfCakes}</h2>
      <button onClick={() => dispatch(buyCake())}>Buy Cakes</button>
    </div>
  );
};

export default HooksCakeContainer;
