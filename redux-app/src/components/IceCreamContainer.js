import React from "react";
import { buyIceCream } from "../redux/Actions/IceCreamAction";
import { connect } from "react-redux";

const IceCreamContainer = (props) => {
  return (
    <div>
      <h2> Number Of IceCreams- {props.numberOfIceCream}</h2>
      <button onClick={props.buyIceCream}>Buy IceCream</button>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    numberOfIceCream: state.iceCream.numberOfIceCream,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    buyIceCream: () => dispatch(buyIceCream()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(IceCreamContainer);
