import React from "react";
import { buyCake } from "../redux/Actions/CakeAction";
import { connect } from "react-redux";

const CakeContainer = (props) => {
  return (
    <div>
      <h2> Number Of Cakes- {props.numberOfCakes}</h2>
      <button onClick={props.buy}>Buy Cake</button>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    numberOfCakes: state.cake.numberOfCakes,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    buy: () => dispatch(buyCake()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CakeContainer);
