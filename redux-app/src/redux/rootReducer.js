import { combineReducers } from "redux";
import Cakereducer from "../redux/Reducer/Cakereducer";
import IceCreamreducer from "../redux/Reducer/IceCreamreducer";

const rootReducer = combineReducers({
  cake: Cakereducer,
  iceCream: IceCreamreducer,
});

export default rootReducer;
