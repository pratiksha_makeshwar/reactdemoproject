import { BUY_CAKE } from "../Types/CakeTypes";

const initialCakeState = {
  numberOfCakes: 10,
};

const Cakereducer = (state = initialCakeState, action) => {
  switch (action.type) {
    case BUY_CAKE:
      return {
        ...state,
        numberOfCakes: state.numberOfCakes - action.payload,
      };
    default:
      return state;
  }
};
export default Cakereducer;
