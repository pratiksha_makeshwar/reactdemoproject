import { BUY_ICECREAM } from "../Types/IceCreamTypes";

const initialIceCreamState = {
  numberOfIceCream: 20,
};

const IceCreamreducer = (state = initialIceCreamState, action) => {
  switch (action.type) {
    case BUY_ICECREAM:
      return {
        ...state,
        numberOfIceCream: state.numberOfIceCream - 1,
      };
    default:
      return state;
  }
};

export default IceCreamreducer;
