import { BUY_ICECREAM } from "../Types/IceCreamTypes";

export const buyIceCream = () => {
  return {
    type: BUY_ICECREAM,
  };
};
