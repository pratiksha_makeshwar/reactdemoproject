import React, { Component } from "react";
import "./App.css";
import CakeContainer from "./components/CakeContainer";
import { Provider } from "react-redux";
import store from "./redux/Store";
import HooksCakeContainer from "./components/HooksCakeContainer";
import IceCreamContainer from "./components/IceCreamContainer";
import NewCakeContainer from "./components/NewCakeContainer";
import ItemContainer from "./components/ItemContainer";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <h1> Hello world</h1>

          <ItemContainer cake />
          <ItemContainer />
          <HooksCakeContainer />
          <CakeContainer />
          <IceCreamContainer />
          <NewCakeContainer />
        </div>
      </Provider>
    );
  }
}

export default App;
