import React from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import { ProgressBar } from "react-bootstrap";
import "./color.css";
import { AiOutlineHeart } from "react-icons/ai";
import { FaBirthdayCake } from "react-icons/fa";
import { ImSpoonKnife } from "react-icons/im";

function Content() {
  return (
    <React.Fragment>
      <h3 className="text-center mt-4  text-black h2">What Do We Offer</h3>
      <div className="div1" id="services">
        <div className="container ms-3">
          <div className="row">
            <h5 className="text-left text-uppercase">Html</h5>

            <div className="prog">
              <ProgressBar variant="danger" now={90} />
            </div>

            <h5 className="text-left text-uppercase">css</h5>
            <div className="prog">
              <ProgressBar varianr="info" now={85} />
            </div>

            <h5 className="text-left">jQuery</h5>
            <div className="prog">
              <ProgressBar variant="warning" now={70} />
            </div>
            <h5 className="text-left">SCSS</h5>
            <div className="prog">
              <ProgressBar variant="bg-light" now={65} />
            </div>

            <h5 className="text-left">Javascript</h5>
            <div className="prog">
              <ProgressBar variant="warning" now={55} />
            </div>

            <h5 className="text-left">PHP</h5>
            <div className="prog">
              <ProgressBar variant="primary" now={50} />
            </div>

            <h5 className="text-left">Angular4/5/6</h5>
            <div className="prog">
              <ProgressBar variant="success" now={45} />
            </div>
          </div>
        </div>
      </div>

      <div className="div2" id="services">
        <span className="body">Web Developer</span>
        <p className="content">
          <AiOutlineHeart size="40px" /> We make tutorial of all the Programming
          and Scripting languages to help for free.
        </p>
        <span className="body">University Important Topics</span>
        <p className="content">
          <FaBirthdayCake size="40px" /> We also covers all the important topics
          as per university to help students to understant topic easily.
        </p>

        <span className="body">Technical Article</span>
        <p className="content">
          <ImSpoonKnife size="40px" /> We write articles on current and
          Interesting Technicals Topic to spread knowledge and sometimes on
          Social Causes too.
        </p>
      </div>
    </React.Fragment>
  );
}
export default Content;
