import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { NavLink } from "react-router-dom";
function Header() {
  return (
    <section className="navbar-dark bg-dark header">
      <div className="container">
        <div className="row">
          <div className="col-sm">
            <nav className="navbar navbar-expand-lg">
              <a className="navbar-brand text-white text-uppercase h1" href="#">
                Thapatechnical
              </a>
              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav mr-auto">
                  <li className="nav-item">
                    <a
                      className="nav-link text-uppercase text-white h6 "
                      href="#services"
                    >
                      services
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link text-uppercase text-white h6"
                      href="#sourcecode"
                    >
                      sourceCode
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Header;
