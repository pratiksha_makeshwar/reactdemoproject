import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./color.css";
import { Carousel } from "react-bootstrap";

function SourceCode() {
  const handleClick = () => {
    window.open("https://www.youtube.com/channel/UCwfaAHy4zQUb2APNOGXUCCA");
  };
  return (
    <React.Fragment>
      <div id="sourcecode">
        <h1 className="t">MyWork And SourceCode</h1>
        <p className="text-center">
          ThapaTechnical works and Youtube videos source code.Just click on any
          vedio and get the source code and we have technical artical also.Have
          a great day.
        </p>

        <div className="photo">
          <h1 className="photo_text">
            Thapa Technical
            <p className="sub">
              House of web Developers<br></br>And technology.
            </p>
          </h1>
          <div className="btn-group">
            <button className="vlog" onClick={handleClick}>
              MY VLOG
            </button>
            <button className="btn">MY BLOGS</button>
          </div>
        </div>

        <div className="img ">
          <img
            title="HTML And CSS Autocomplete Plugin in sublime text 3"
            src="https://th.bing.com/th/id/R.d58339de61b5d07fd4db988f8e8a7cdd?rik=1%2fUoNcqqIfTiXw&riu=http%3a%2f%2fthewowstyle.com%2fwp-content%2fuploads%2f2015%2f01%2fnature-images..jpg&ehk=%2fG27fwqbQI%2fi%2bxgGclM1BHuObngpvsp27tc36F59T9c%3d&risl=&pid=ImgRaw&r=0"
            width={300}
            height={300}
          />
          <img
            className="ms-3"
            title="How To Download and Install Sublime editor text 3"
            src="https://tse4.mm.bing.net/th/id/OIP.v7gEKUNUq-Jy-ARL41hoxAHaE7?pid=ImgDet&rs=1"
            width={300}
            height={300}
            alt="HTML"
          />

          <img
            className="ms-3"
            title="How to create school TimeTable in HTML"
            src="https://th.bing.com/th/id/R.af030d043073174011fe4d10ca1f37c7?rik=t5Okq4S3ijbd3A&riu=http%3a%2f%2ftemplatelab.com%2fwp-content%2fuploads%2f2015%2f10%2fWork-Schedule-Template-14.jpg%3fw%3d320&ehk=yFHUwiIJM9Frzcz2N3MYBKNM2mTH73LhbtrQVF8wdYg%3d&risl=&pid=ImgRaw&r=0"
            width={300}
            height={300}
            alt="HTML"
          />
          <img
            className="ms-3"
            title="Latest Technological Adavancement In Farming and Agriculture"
            src="https://tse4.mm.bing.net/th/id/OIP.Pa86cxSaffq6gEfh11OCdAHaE8?pid=ImgDet&rs=1"
            width={300}
            height={300}
          />

          <img
            className="mt-2"
            title="Why I Choose YouTube To Help Others"
            src="https://tse4.mm.bing.net/th/id/OIP.SY5zTPBfLCbZmOIhHYNISwHaFZ?pid=ImgDet&rs=1"
            width={300}
            height={300}
          />

          <img
            className="mt-2 ms-3"
            title="Web Developement Folder Structure"
            src="https://tse4.mm.bing.net/th/id/OIP.1KaYaEtsZo6NScP7lRxFwgHaHk?pid=ImgDet&rs=1"
            width={300}
            height={300}
          />

          <img
            className="mt-2 ms-3"
            title="IOT Technology Based HealthCare Products"
            src="https://www.froedtert.com/sites/default/files/styles/one_column/public/image/2018-09/be-an-engaged-health-care-consumer.jpg"
            width={300}
            height={300}
          />
        </div>

        <div style={{ paddingTop: 50 }}>
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <Carousel>
                  <Carousel.Item style={{ height: "400px" }}>
                    <img
                      style={{ height: "400px" }}
                      className="d-block w-100 "
                      src="https://tse4.mm.bing.net/th/id/OIP.bZvQECb54LMncsjzDoZxoAHaEK?pid=ImgDet&rs=1"
                      alt="First slide"
                    />
                    <Carousel.Caption>
                      <h5>
                        "Vinod Bahadar Thapa aka ThapaTechnical is a web
                        developer and<br></br>Youtuber. ThapaTechnical want to
                        help others by providing free videos on web<br></br>
                        Development and school/University important topics."
                      </h5>
                      <p className="slide">
                        Vinod Bahadar Thapa,CEO of ThapaTechnical
                      </p>
                    </Carousel.Caption>
                  </Carousel.Item>

                  <Carousel.Item style={{ height: "400px" }}>
                    <img
                      style={{ height: "400px" }}
                      className="d-block w-100"
                      src="https://tse4.mm.bing.net/th/id/OIP.bZvQECb54LMncsjzDoZxoAHaEK?pid=ImgDet&rs=1"
                      alt="Second slide"
                    />

                    <Carousel.Caption>
                      <h5>
                        "Vinod Bahadar Thapa aka ThapaTechnical is a web
                        developer and<br></br>Youtuber. ThapaTechnical want to
                        help others by providing free videos on web<br></br>
                        Development and school/University important topics."
                      </h5>
                      <p className="slide">
                        Vinod Bahadar Thapa,CEO of ThapaTechnical
                      </p>
                    </Carousel.Caption>
                  </Carousel.Item>

                  <Carousel.Item style={{ height: "400px" }}>
                    <img
                      style={{ height: "400px" }}
                      className="d-block w-100"
                      src="https://tse4.mm.bing.net/th/id/OIP.bZvQECb54LMncsjzDoZxoAHaEK?pid=ImgDet&rs=1"
                      alt="Third slide"
                    />
                    <Carousel.Caption>
                      <h5>
                        "Vinod Bahadar Thapa aka ThapaTechnical is a web
                        developer and<br></br>Youtuber. ThapaTechnical want to
                        help others by providing free videos on web<br></br>
                        Development and school/University important topics."
                      </h5>
                      <p className="slide">
                        Vinod Bahadar Thapa,CEO of ThapaTechnical
                      </p>
                    </Carousel.Caption>
                  </Carousel.Item>
                </Carousel>
              </div>
            </div>
          </div>
        </div>

        <h2 className="pt-3 text-center"> Get in touch</h2>
        <p className="text-center">Feel free to drop us a line to contact us</p>
      </div>
    </React.Fragment>
  );
}

export default SourceCode;
