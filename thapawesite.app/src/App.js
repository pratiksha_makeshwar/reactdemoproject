import "./App.css";
import React from "react";
import Header from "./components/Header";
import Content from "./components/Content";

import SourceCode from "./components/SourceCode";

function App() {
  return (
    <React.Fragment>
      <Header />
      <Content />
      <SourceCode />
    </React.Fragment>
  );
}

export default App;
