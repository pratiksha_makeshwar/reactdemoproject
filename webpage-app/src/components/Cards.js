import React from "react";
import CardItem from "./CardItem";
import "./Cards.css";

function Cards() {
  return (
    <div className="cards">
      <h1>Check out these EPIC Destinations!</h1>
      <div className="cards__container">
        <div className="cards__wrapper">
          <ul className="cards__items">
            <CardItem
              src="Images/waterfall.jpeg"
              text="Explore the hidden waterfall deep inside the Amazon Jungle"
              label="Advanture"
              path="/services"
            />
            <CardItem
              src="Images/Island.jpeg"
              text="Travel through the Island of Bali in a Private Cruise"
              label="Luxury"
              path="/services"
            />
          </ul>
          <ul className="cards__items">
            <CardItem
              src="Images/ocean.jpeg"
              text="Set Sail in the Atlantic Ocean visiting Uncharted Waters"
              label="Mystery"
              path="/services"
            />

            <CardItem
              src="Images/Peak.jpeg"
              text="Experience Football on Top of the Himilayan Mountains"
              label="Adventure"
              path="/products"
            />
            <CardItem
              src="Images/Desert.jpeg"
              text="Ride through the Sahara Desert on a guided camel tour "
              label="Adrenaline"
              path="/sign-up"
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;
