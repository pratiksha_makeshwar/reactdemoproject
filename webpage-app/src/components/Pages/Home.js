import React from "react";
import "../../App.css";
import Background from "../Background";
import Cards from "../Cards";
import Footer from "../Footer";

function Home() {
  return (
    <div>
      <Background />
      <Cards />
      <Footer />
    </div>
  );
}

export default Home;
